const express = require("express");
const app = express();
const cors = require("cors");
const bodyParser = require('body-parser');
var xss = require('xss-clean');
const helmet = require("helmet");
const rateLimit = require('express-rate-limit');
const path = require('path');
const mysql = require('mysql');
app.use(bodyParser.json());
app.use(express.urlencoded({ extended: true }));

// Configuring the database
const dbConfig = require('./config/database.config');


app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.use(cors());

// to prevent ddos attack 
const limit = rateLimit({
  max: 100,// max requests
  windowMs: 60 * 60 * 1000, // 1 Hour of ban each ip depass 100 request  
  message: 'Too many requests !' // message to send
});

global.ddos = limit;
global.__basedir = __dirname;

// XSS attacks
app.use(xss());

//helmet protection lol helmet LVL 3 HAHAHAHA
app.use(helmet.contentSecurityPolicy());
app.use(helmet.dnsPrefetchControl());
app.use(helmet.frameguard());
app.use(helmet.hidePoweredBy());
app.use(helmet.hsts());
app.use(helmet.ieNoOpen());
app.use(helmet.noSniff());
app.use(helmet.permittedCrossDomainPolicies());
app.use(helmet.referrerPolicy());
app.use(helmet.xssFilter());

app.use("/", express.static(path.join(__dirname, "/app/img")));




const port = process.env.PORT || 8080;

//Route Telephone

const TelephoneRoutes = require("./routes/telephone")(app);

module.exports = app.listen(port, () => {
    console.log(`app listening at http://localhost:${port}`)
  })