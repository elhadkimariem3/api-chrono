var dbConn = require('../config/database.config');

exports.func = (req, res) => {

    dbConn.query('SELECT  * FROM `phone_trades` pt INNER JOIN `phone_models` pm on pt.ID=pm.PHONE_TRADE_ID INNER JOIN `phones` p on p.PHONE_MODEL_ID=pm.ID order by NAME_MODEL', async (err, result) => {
        if (!err) {
            obj = {
                data: result
            }
            const seen = new Set();
            let objPhone = [];

            for (let i = 0; i < obj.data.length; i++) {
                objPhone.push({ name: obj.data[i].NAME_MODEL, index: [], NAME_TRADE: obj.data[i].NAME_TRADE });

            }

            let filteredArr = await objPhone.filter(el => {
                const duplicate = seen.has(el.name);
                seen.add(el.name);
                return !duplicate;
            })


            for (let j = 0; j < filteredArr.length; j++) {

                obj.data.filter(function (yourArray, index) {

                    if (yourArray.NAME_MODEL === filteredArr[j].name) {

                        filteredArr[j].index.push(index)
                    }
                })


            }

            let arrDelivery = []

            for (let k = 0; k < filteredArr.length; k++) {
                if (filteredArr[k].index.length == 1) {
                    arrDelivery.push(obj.data[filteredArr[k].index[0]])
                }
                else if (filteredArr[k].index.length > 1) {
                    let tempOtherSpecs = []
                    for (let indexOfPhone = 0; indexOfPhone < filteredArr[k].index.length; indexOfPhone++) {
                        tempOtherSpecs.push({ STORAGE: obj.data[filteredArr[k].index[indexOfPhone]].STORAGE, PA_BASE: obj.data[filteredArr[k].index[indexOfPhone]].PA_BASE })


                    }
                    arrDelivery.push({
                        NAME_TRADE: filteredArr[k].NAME_TRADE,
                        NAME_MODEL: filteredArr[k].name,
                        phones: tempOtherSpecs

                    })

                }

            }
            res.json(arrDelivery);

        } else {

            console.log(err);
        }
    });
}
